# Imagen en la cual está basada esta imagen
# https://docs.docker.com/engine/reference/builder/#from
FROM openjdk:11-jre-slim

# Estos son parámetros que se le pueden pasar al cliente de 
# Docker al crear (construir) esta imagen, tienen valores por
# defecto: https://docs.docker.com/engine/reference/builder/#arg
ARG user=spring
ARG group=spring
ARG uid=1000
ARG gid=1000
ARG JAR_FILE=target/*.jar

# Esto es un comando que se ejecuta sobre la imagen base,
# la imagen resultante se usará en los siguientes pasos
# https://docs.docker.com/engine/reference/builder/#run
RUN groupadd -g ${gid} ${group} && useradd -u ${uid} -g ${group} -s /bin/sh ${user}

# Este es el usuario bajo el cual se ejecutarán las siguientes
# instrucciones, a partir de este punto
# https://docs.docker.com/engine/reference/builder/#user
USER spring:spring

# Este comando copia un archivo fuera de la imagen hacia una 
# ruta dentro de la nueva imagen https://docs.docker.com/engine/reference/builder/#copy
COPY ${JAR_FILE} app.jar

# Permite definir la acción (ejecutable) que correrá al
# ser invocado como contenedor la imagen creada
# https://docs.docker.com/engine/reference/builder/#entrypoint
ENTRYPOINT ["java","-jar","/app.jar"]
