package is.proyecto.backend.service;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import is.proyecto.backend.data.DivisionRepository;
import is.proyecto.backend.data.GrupoRepository;
import is.proyecto.backend.dto.GrupoDto;
import is.proyecto.backend.dto.GrupoMapper;
import is.proyecto.backend.dto.PostDto;
import is.proyecto.backend.dto.PostMapper;
import is.proyecto.backend.model.Division;
import is.proyecto.backend.model.Grupo;
import is.proyecto.backend.model.Post;
import lombok.extern.slf4j.Slf4j;

/** The Constant log. */
@Slf4j
@Service
public class GrupoService {

	/** The grupo repository. */
	@Autowired
	private GrupoRepository grupoRepository;
	
	/** The post service */
	@Autowired
	private PostService postService;

	/** The grupo mapper. */
	@Autowired
	private GrupoMapper grupoMapper;
	
	@Autowired
	private PostMapper postMapper;

	/** The Division Repository **/
	@Autowired
	private DivisionRepository divisionRepository;

	/**
	 * Agrega un nuevo grupo
	 *
	 * @param grupoDto Recibe por parametro un objeto GrupoDto
	 * @return Regresa un objeto de tipo GrupoDto
	 */
	@Transactional
	public GrupoDto create(GrupoDto grupoDto) {

		Grupo grupo = grupoMapper.fromDto(grupoDto);

		Optional<Division> optDivision = divisionRepository.findById(grupoDto.getDivision());

		if (optDivision.isEmpty()) {
			throw new IllegalArgumentException("Division con las siglas " + grupoDto.getDivision() + " no encontrada");
		}

		Division division = optDivision.get();
		grupo.setDivision(division);

		grupo = grupoRepository.save(grupo);

		grupoDto.setGroupId(grupo.getGroupId());

		return grupoDto;
	}

	/**
	 * Find all.
	 *
	 * @return the iterable
	 */
	public Iterable<GrupoDto> findAll() {

		Iterable<Grupo> resultsIterable = grupoRepository.findAll();

		List<GrupoDto> results = new ArrayList<>();

		for (Grupo grupo : resultsIterable) {
			GrupoDto grupoDto = grupoMapper.toDto(grupo);

			results.add(grupoDto);

		}

		return results;
	}

	/**
	 * Encuentra un grupo mediante su id
	 *
	 * @param id El id del grupo
	 * @return the optional si es que existe
	 */
	public Optional<GrupoDto> findById(Integer groupId) {

		Optional<Grupo> optGrupo = grupoRepository.findById(groupId);

		GrupoDto grupoDto = null;

		if (optGrupo.isPresent()) {
			Grupo grupo = optGrupo.get();

			grupoDto = grupoMapper.toDto(grupo);
		}

		return Optional.ofNullable(grupoDto);
	}

	/**
	 * Update.
	 *
	 * @param id      the id
	 * @param updates the updates
	 * @return the grupo dto
	 */
	@Transactional
	public GrupoDto update(Integer groupId, Map<String, Object> updates) {

		Optional<Grupo> optGrupo = grupoRepository.findById(groupId);

		if (optGrupo.isEmpty()) {
			return null;
		}
		Grupo grupo = optGrupo.get();

		for (String key : updates.keySet()) {
			try {
				if (!key.equals("groupId")) {
					Method setter = new PropertyDescriptor(key, Grupo.class).getWriteMethod();
					setter.invoke(grupo, updates.get(key));
				}
			} catch (Exception ex) {
				log.info("Could not set key " + key + " value: " + updates.get(key) + " type "
						+ updates.get(key).getClass() + " exception:" + ex.getMessage());
			}
		}

		grupo = grupoRepository.save(grupo);

		return grupoMapper.toDto(grupo);

	}

	/**
	 * Delete.
	 *
	 * @param id the id of the division to delete
	 * @return true, if successful, false if category not found in database
	 */
	@Transactional
	public boolean delete(Integer groupId) {
		Optional<Grupo> optGrupo = grupoRepository.findById(groupId);
		if (optGrupo.isEmpty()) {
			return false;
		}
		grupoRepository.delete(optGrupo.get());

		return true;

	}

	@Transactional
	public GrupoDto addPostToGroup(GrupoDto grupoDto, PostDto postDto) {
		
		Grupo grupo = grupoRepository.findById(grupoDto.getGroupId()).get();
		
		postDto = postService.create(postDto);
		
		Post post = postMapper.fromDto(postDto);
		
		grupo.addPost(post);
		
		grupo = grupoRepository.save(grupo);

		return grupoMapper.toDto(grupo);
	}
	
	@Transactional
	public Iterable<PostDto> getPostsOfGroup(Integer grupoId) {
		
		Optional<Grupo> optGrupo = grupoRepository.findById(grupoId);
		
		Grupo grupo;
		List <PostDto> posts = new ArrayList<>();
		
		if (optGrupo.isEmpty()) {
			return null;
		}
		
		grupo = optGrupo.get();
		
		for (Post post : grupo.getPosts()) {
			posts.add(postMapper.toDto(post));
		}
		
		return posts;
	}

}
