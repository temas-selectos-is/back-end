package is.proyecto.backend.service;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import is.proyecto.backend.data.PostRepository;
import is.proyecto.backend.dto.PostDto;
import is.proyecto.backend.dto.PostMapper;
import is.proyecto.backend.model.Post;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PostService {

	@Autowired
	private PostRepository postRepository;

	@Autowired
	private PostMapper postMapper;

	/**
	 * Creates the.
	 *
	 * @param postDto the post dto
	 * @return the post dto
	 */
	@Transactional
	public PostDto create(PostDto postDto) {
		
		Optional<Post> optPost = postRepository.findById(postDto.getId());
		
		Post post;
		
		if (optPost.isPresent()) {
			post = optPost.get();
		} else {
			post = Post.builder().contenido(postDto.getContenido()).fecha(new DateTime(Instant.now())).build();
		}

		post = postRepository.save(post);
		
		return postMapper.toDto(post);
	}

	/**
	 * Find all.
	 *
	 * @return the iterable
	 */
	public Iterable<PostDto> findAll() {

		Iterable<Post> resultsIterable = postRepository.findAll();

		List<PostDto> results = new ArrayList<>();

		for (Post post : resultsIterable) {
			PostDto postDto = postMapper.toDto(post);

			results.add(postDto);

		}

		return results;
	}

	/**
	 * Find.
	 *
	 * @param id the id
	 * @return the optional
	 */
	public Optional<PostDto> find(Integer id) {

		Optional<Post> optPost = postRepository.findById(id);

		PostDto postDto = null;

		if (optPost.isPresent()) {
			Post post = optPost.get();

			postDto = postMapper.toDto(post);
		}

		return Optional.ofNullable(postDto);
	}

	/**
	 * Update.
	 *
	 * @param id      the id
	 * @param updates the updates
	 * @return the post dto
	 */
	@Transactional
	public PostDto update(Integer id, Map<String, Object> updates) {

		Optional<Post> optPost = postRepository.findById(id);
		if (optPost.isEmpty()) {
			return null;
		}

		Post post = optPost.get();

		for (String key : updates.keySet()) {
			try {
				if (!key.equals("id")) {

					if (key.equals("postsQueLoCitan")) {

					}
					Method setter = new PropertyDescriptor(key, Post.class).getWriteMethod();

					setter.invoke(post, updates.get(key));
				}
			} catch (Exception ex) {
				log.info("No se pudo actualizar la propiedad " + key + " valor: " + updates.get(key) + " tipo "
						+ updates.get(key).getClass() + " excepción:" + ex.getMessage());
			}
		}

		post = postRepository.save(post);

		return postMapper.toDto(post);

	}

	/**
	 * Delete.
	 *
	 * @param id the id of the post to delete
	 * @return true, if successful, false if category not found in database
	 */
	@Transactional
	public boolean delete(Integer id) {

		Optional<Post> optPost = postRepository.findById(id);

		if (optPost.isEmpty()) {
			return false;
		}

		postRepository.delete(optPost.get());

		return true;

	}

	@Transactional
	public boolean addQuotingPostToPost(Integer postQuotedId, Integer postQuotingId) {

		Optional<Post> optQuotedPost = postRepository.findById(postQuotedId);
		Optional<Post> optQuotingPost = postRepository.findById(postQuotingId);

		if (optQuotingPost.isEmpty()) {
			return false;
		}

		Post quotedPost = optQuotedPost.get();
		Post quotingPost = optQuotingPost.get();

		quotedPost.addQuotingPost(quotingPost);

		postRepository.save(quotedPost);

		return true;

	}

}
