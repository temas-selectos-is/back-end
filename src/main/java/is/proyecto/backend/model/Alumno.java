package is.proyecto.backend.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new alumno.
 *
 * @param id        the id
 * @param correo    the correo
 * @param matricula the matricula
 * @param password  the password
 * @param division  the division
 */
@AllArgsConstructor

/**
 * Instantiates a new alumno.
 */
@NoArgsConstructor

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Builder
@Entity
public class Alumno {

	/** The id. */
	@Id
	@GeneratedValue
	private Integer id;

	/** The correo. */
	@Column(nullable = false, unique = true)
	private String correo;

	/** The matricula. */
	@Column(nullable = false, unique = true)
	private String matricula;

	/** The password. */
	@Column(nullable = false)
	private String password;

	/** The division. */
	@ManyToOne
	private Division division;

	/** The posts. */
	@OneToMany(targetEntity = Post.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE, orphanRemoval = true)
	private final List<Post> posts = new ArrayList<>();

	/** The groups. */
	@OneToMany(targetEntity = Grupo.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE, orphanRemoval = true)
	private final List<Grupo> groups = new ArrayList<>();

}
