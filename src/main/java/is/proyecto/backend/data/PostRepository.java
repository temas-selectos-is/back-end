package is.proyecto.backend.data;

import org.springframework.data.repository.CrudRepository;

import is.proyecto.backend.model.Post;

public interface PostRepository extends CrudRepository<Post, Integer> {

}
