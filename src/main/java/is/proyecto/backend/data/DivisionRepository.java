package is.proyecto.backend.data;

import org.springframework.data.repository.CrudRepository;

import is.proyecto.backend.model.Division;

public interface DivisionRepository extends CrudRepository<Division, String> {

}
