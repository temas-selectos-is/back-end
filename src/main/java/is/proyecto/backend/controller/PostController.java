package is.proyecto.backend.controller;

import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import is.proyecto.backend.dto.PostDto;
import is.proyecto.backend.service.PostService;

/**
 * The Class PostService.
 */
@RestController
@RequestMapping("/v1")
@CrossOrigin(origins = "*")
@Api(value = "post")
public class PostController {

	/** The post service. */
	@Autowired
	private PostService postService;

	/**
	 * Creates a post
	 *
	 * @param postDto the post dto to create
	 * @return the response entity created
	 */
	@ApiOperation(value = "Crea un nuevo post", notes = "Permite crear un nuevo post")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Post creado exitosamente", response = PostDto.class),
			@ApiResponse(code = 400, message = "Post inválido", response = PostDto.class),
			@ApiResponse(code = 500, message = "Error en el servidor al crear el post") })
	@PostMapping(path = "/posts", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PostDto> create(
			@RequestBody @Valid @ApiParam(name = "post", value = "Post a ser creado", required = true) PostDto postDto) {

		try {
			postDto = postService.create(postDto);
		} catch (Exception e) {
			throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
		if (postDto == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(postDto);
		}

		return ResponseEntity.status(HttpStatus.CREATED).body(postDto);
	}

	/**
	 * Retrieve a post by its id
	 *
	 * @param id the id
	 * @return the response entity if found
	 */
	@ApiOperation(value = "Obtiene un post por su id", notes = "Obtiene un post por su id")
	@GetMapping(path = "/posts/{idPost}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Post encontrado", response = PostDto.class),
			@ApiResponse(code = 404, message = "Post no encontrado") })
	public ResponseEntity<Object> retrieve(
			@ApiParam(name = "idPost", value = "Id del post", required = true) @PathVariable(name = "idPost", required = true) Integer id) {

		Optional<PostDto> optPostDto = postService.find(id);

		if (optPostDto.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id " + id + " no encontrado");
		}

		return ResponseEntity.status(HttpStatus.OK).body(optPostDto.get());

	}

	/**
	 * Patch a post
	 *
	 * @param updates the updates to perform over the post
	 * @param id      the id of the post
	 * @return the response entity if it exists
	 */
	@ApiOperation(value = "Actualiza valores de un post", notes = "Actualiza los valores de un post (a excepción de su id)")
	@PatchMapping(path = "/posts/{idPost}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Post actualizado exitosamente", response = PostDto.class),
			@ApiResponse(code = 400, message = "Post no encontrado"),
			@ApiResponse(code = 409, message = "El post no puede ser actualizado en este momento") })
	public ResponseEntity<Object> patch(@RequestBody Map<String, Object> updates,
			@ApiParam(name = "idPost", value = "Id del post", required = true) @PathVariable(name = "idPost", required = true) Integer id) {

		PostDto postDto = null;

		try {
			postDto = postService.update(id, updates);

			if (postDto == null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Id " + id + " no encontrado");
			}

		} catch (Exception ex) {
			throw new HttpClientErrorException(HttpStatus.CONFLICT, ex.getMessage());
		}

		return ResponseEntity.status(HttpStatus.OK).body(postDto);

	}

	/**
	 * Delete.
	 *
	 * @param id the id of the post
	 * @return status according if deleted sucessfully or not
	 */
	@ApiOperation(value = "Borra un post", notes = "Borra un post según sus id")
	@DeleteMapping(path = "/posts/{idPost}")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Post borrado exitosamente"),
			@ApiResponse(code = 400, message = "Post no encontrado"),
			@ApiResponse(code = 409, message = "El post no puede ser borrado en este momento") })
	public ResponseEntity<Object> delete(
			@ApiParam(name = "idPost", value = "Siglas de la división", required = true) @PathVariable(name = "idPost", required = true) Integer id) {

		boolean retval;

		try {
			retval = postService.delete(id);

			if (!retval) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Id " + id + " no encontrado");
			}

		} catch (Exception ex) {

			throw new HttpClientErrorException(HttpStatus.CONFLICT, ex.getMessage());
		}

		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

	}

	/**
	 * Add a post to its quoted by
	 *
	 * @param id the id
	 * @return the response entity if found
	 */
	@ApiOperation(value = "Agrega post que cita", notes = "Agrega un post a la lista de posts que lo citan de otro post")
	@PostMapping(path = "/posts/{idPost}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Post que cita agregado"),
			@ApiResponse(code = 400, message = "Post que cita inválido"),
			@ApiResponse(code = 404, message = "Post no encontrado"),
			@ApiResponse(code = 409, message = "Error agregando post que cita") })
	public ResponseEntity<Object> addQuotingPost(
			@ApiParam(name = "idPost", value = "Id del post", required = true) @PathVariable(name = "idPost", required = true) Integer id,
			@RequestBody @Valid @ApiParam(name = "post", value = "Post que cita al otro post", required = true) PostDto postDto) {

		Optional<PostDto> optPostDtoCitado = postService.find(id);

		if (optPostDtoCitado.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id del post citado " + id + " no encontrado");
		}

		PostDto postDtoCitado = optPostDtoCitado.get();

		try {
			if (postService.addQuotingPostToPost(postDtoCitado.getId(), postDto.getId())) {
				return ResponseEntity.status(HttpStatus.OK).build();
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Post que cita inválido");
			}
		} catch (Exception ex) {
			throw new HttpClientErrorException(HttpStatus.CONFLICT, ex.getMessage());
		}

	}

}
