package is.proyecto.backend.controller;

import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import is.proyecto.backend.dto.DivisionDto;
import is.proyecto.backend.dto.GrupoDto;
import is.proyecto.backend.dto.PostDto;
import is.proyecto.backend.service.GrupoService;

/**
 * The Class GrupoController.
 */
@RestController
@RequestMapping("/v1")
@CrossOrigin(origins = "*")
@Api(value = "grupo")
public class GrupoController {

	@Autowired
	private GrupoService grupoService;

	@ApiOperation(value = "Crea un nuevo grupo", notes = "Permite crear un nuevo grupo, el groupId de los grupos es único")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Grupo creado exitosamente", response = DivisionDto.class),
			@ApiResponse(code = 400, message = "Grupo inválido", response = GrupoDto.class),
			@ApiResponse(code = 500, message = "Error en el servidor al crear grupo") })
	@PostMapping(path = "/grupos", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GrupoDto> create(
			@RequestBody @Valid @ApiParam(name = "grupo", value = "Grupo a ser creado", required = true) GrupoDto grupoDto) {

		try {
			grupoDto = grupoService.create(grupoDto);
		} catch (Exception e) {
			throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
		if (grupoDto == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(grupoDto);
		}

		return ResponseEntity.status(HttpStatus.CREATED).body(grupoDto);
	}

	@ApiOperation(value = "Obtiene todos los grupos", notes = "Obtiene una lista de todos los grupos actualmente en el sistema")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Grupos obtenidos", response = GrupoDto.class, responseContainer = "List") })
	@GetMapping(path = "/grupos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> retrieve() {

		Iterable<GrupoDto> results = grupoService.findAll();

		return ResponseEntity.status(HttpStatus.OK).body(results);

	}

	@ApiOperation(value = "Obtiene un grupo por su groupId", notes = "Obtiene un grupo según su groupId")
	@GetMapping(path = "/grupos/{groupId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Grupo encontrado", response = GrupoDto.class),
			@ApiResponse(code = 404, message = "Grupo no encontrado") })
	public ResponseEntity<?> retrieve(
			@ApiParam(name = "groupId", value = "Id del grupo", required = true) @PathVariable(name = "groupId", required = true) Integer groupId) {

		Optional<GrupoDto> optGrupoDto = grupoService.findById(groupId);

		if (optGrupoDto.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id " + groupId + " not found");
		}

		return ResponseEntity.status(HttpStatus.OK).body(optGrupoDto.get());

	}

	@ApiOperation(value = "Actualiza valores de un Grupo", notes = "Actualiza los valores de un grupo(a excepción de su id)")
	@PatchMapping(path = "/grupos/{groupId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Grupo actualizado exitosamente", response = GrupoDto.class),
			@ApiResponse(code = 400, message = "Grupo no encontrado"),
			@ApiResponse(code = 409, message = "El grupo no puede ser actualizado en este momento") })
	public ResponseEntity<Object> patch(@RequestBody Map<String, Object> updates,

			@ApiParam(name = "groupId", value = "id del Grupo", required = true) @PathVariable(name = "groupId", required = true) Integer groupId) {

		GrupoDto grupoDto = null;

		try {
			grupoDto = grupoService.update(groupId, updates);

			if (grupoDto == null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Id " + groupId + " not found");
			}

		} catch (Exception ex) {
			throw new HttpClientErrorException(HttpStatus.CONFLICT, ex.getMessage());
		}

		return ResponseEntity.status(HttpStatus.OK).body(grupoDto);

	}

	@ApiOperation(value = "Borra un grupo", notes = "Borra un grupo según su id")
	@DeleteMapping(path = "/grupos/{groupId}")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Grupo borrado exitosamente"),
			@ApiResponse(code = 400, message = "Grupo no encontrado"),
			@ApiResponse(code = 409, message = "El grupo no puede ser borrada en este momento") })
	public ResponseEntity<Object> delete(
			@ApiParam(name = "groupId", value = "Id del grupo", required = true) @PathVariable(name = "groupId", required = true) Integer groupId) {

		boolean retval;

		try {
			retval = grupoService.delete(groupId);

			if (!retval) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id " + groupId + " not found");
			}

		} catch (Exception ex) {

			throw new HttpClientErrorException(HttpStatus.CONFLICT, ex.getMessage());
		}

		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

	}

	@ApiOperation(value = "Agrega post a grupo")
	@PostMapping(path = "/grupos/{groupId}/posts", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> addPostToGroup(
			@ApiParam(name = "groupId", value = "Id del grupo", required = true) @PathVariable(name = "groupId", required = true) Integer groupId,
			@RequestBody @Valid @ApiParam(name = "post", value = "Post a ser creado", required = true) PostDto postDto){
		
		GrupoDto grupoDto;
		
		Optional <GrupoDto> optGrupoDto = grupoService.findById(groupId);
		
		if (optGrupoDto.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id " + groupId + " not found");
		} else {
			grupoDto = optGrupoDto.get();
		}
				
		try {
			grupoDto = grupoService.addPostToGroup(grupoDto, postDto);
			return ResponseEntity.status(HttpStatus.OK).body(grupoDto);
		} catch (Exception e) {
			throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}

	}
	
	@ApiOperation(value = "Obtiene los posts de un grupo", notes = "Obtiene todos los posts asociados a un grupo")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Grupos obtenidos", response = PostDto.class, responseContainer = "List"),
			@ApiResponse(code = 404, message = "Grupo no encontrado")})
	@GetMapping(path = "/grupos/{groupId}/posts", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getGroupPosts(
			@ApiParam(name = "groupId", value = "Id del grupo", required = true) @PathVariable(name = "groupId", required = true) Integer groupId) {

		Iterable <PostDto> posts = grupoService.getPostsOfGroup(groupId);
		
		if (posts != null) {
			return ResponseEntity.status(HttpStatus.OK).body(posts);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id " + groupId + " not found");
		}

	}	
	
}
