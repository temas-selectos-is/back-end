package is.proyecto.backend.dto;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import is.proyecto.backend.model.Alumno;

@Mapper(componentModel = "spring", uses = { DivisionMapper.class, GrupoMapper.class, PostMapper.class })
public interface AlumnoMapper {

	AlumnoMapper INSTANCE = Mappers.getMapper(AlumnoMapper.class);

	AlumnoDto toDto(Alumno alumno);

	Alumno fromDto(AlumnoDto dto);

	default Integer asInteger(Alumno alumno) {
		return alumno.getId();
	}

	Alumno fromInteger(Integer id);

}
